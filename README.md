# README #

Ça peut ne plus fonctionner d'un moment à l'autre mais il faut savoir prendre des risques dans la vie, nom d'un tonneau en bois !

### Exemple d'utilisation ###


```
#!PHP
<?php
require_once('overwatchdata/OverwatchParser.class.php');
$op = new OverwatchParser('Fadden-2118');
$data = $op->parse();
if($data != null)
{
	echo '<ul>';
	echo '<li>' . $data->playerName . ' : ' . $data->competitiveRank . '</li>';
	echo '<li>' . $data->quickPlay->healingDoneAverage . '</li>';
	echo '<li>' . $data->quickPlay->soloKillsAverage . ' - ' . $data->competitivePlay->soloKillsAverage . '</li>';
	echo '</ul>';
	echo '<img src="' . $data->playerLevelIcon . '" alt="Level" />';
	echo '<img src="' . $data->competitivePlay->heroes[0]->bigImage() . '" alt="' . $data->competitivePlay->heroes[0]->name . '" />';
	echo '<img src="' . $data->competitivePlay->heroes[0]->photo . '" alt="' . $data->competitivePlay->heroes[0]->name . '" />';
	echo '<p>' . $data->quickPlay->heroes[1]->minutes . '</p>';
}
else
{
	echo '<p>Le joueur n\'existe pas.</p>';
}
?>
```

Veuillez vous référer à la documentation toute fraîche pour de plus amples renseignements.

![Barrels2.jpg](https://bitbucket.org/repo/pLEd4o/images/4277482256-Barrels2.jpg)

*Tonneaux en bois en rang d'oignons*

*(Photo : www.CGPGrey.com [CC BY 2.0 (http://creativecommons.org/licenses/by/2.0)], via Wikimedia Commons)*