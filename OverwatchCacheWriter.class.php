<?php

/**
 * Génère le cache
 */
class OverwatchCacheWriter
{
	/**
	 * Génère le fichier de cache
	 * @param OverwatchData $data Données à mettre en cache
	 * @param string $file Fichier de cache à créer
	 */
	public function write(OverwatchData $data, $file)
	{
		$str = '<cache>';
		
		// General
		$str .= '<h1 class="header-masthead">' . $data->playerName . '</h1>';
		$str .= '<div style="background-image:url(' . $data->playerLevelIcon . ')" class="player-level"><div>' . $data->playerLevel . '</div></div>';
		$str .= '<div class="competitive-rank"><div>' . $data->competitiveRank . '</div></div>';
		$str .= '<p class="masthead-detail"><span>' . $data->gamesWon . '</span></p>';

		// QuickPlay 
		$str .= '<div id="quickplay">';
		
		$str .= '<div class="card-heading">' . $data->quickPlay->eliminationsAverage . '</div>';
		$str .= '<div class="card-heading">' . $data->quickPlay->damageDoneAverage . '</div>';
		$str .= '<div class="card-heading">' . $data->quickPlay->deathsAverage . '</div>';
		$str .= '<div class="card-heading">' . $data->quickPlay->finalBlowsAverage . '</div>';
		$str .= '<div class="card-heading">' . $data->quickPlay->healingDoneAverage . '</div>';
		$str .= '<div class="card-heading">' . $data->quickPlay->objectiveKillsAverage . '</div>';
		$str .= '<div class="card-heading">' . $data->quickPlay->objectiveTimeAverage . '</div>';
		$str .= '<div class="card-heading">' . $data->quickPlay->soloKillsAverage . '</div>';
		
		$str .= '<div class="hero-comparison-section"><div class="progress-category">';
		foreach($data->quickPlay->heroes as $hero)
		{
			$str .= '<div class="progress-category-item">';
				$str .= '<img src="' . $hero->photo . '" />';
				$str .= '<div class="title">' . $hero->name . '</div>';
				$str .= '<div class="description">' . $hero->minutes . '</div>';
			$str .= '</div>';
		}
		$str .= '</div></div>';
		
		$str .= '</div>';

		// Competitive
		$str .= '<div id="competitive">';
		
		$str .= '<div class="card-heading">' . $data->competitivePlay->eliminationsAverage . '</div>';
		$str .= '<div class="card-heading">' . $data->competitivePlay->damageDoneAverage . '</div>';
		$str .= '<div class="card-heading">' . $data->competitivePlay->deathsAverage . '</div>';
		$str .= '<div class="card-heading">' . $data->competitivePlay->finalBlowsAverage . '</div>';
		$str .= '<div class="card-heading">' . $data->competitivePlay->healingDoneAverage . '</div>';
		$str .= '<div class="card-heading">' . $data->competitivePlay->objectiveKillsAverage . '</div>';
		$str .= '<div class="card-heading">' . $data->competitivePlay->objectiveTimeAverage . '</div>';
		$str .= '<div class="card-heading">' . $data->competitivePlay->soloKillsAverage . '</div>';
		
		$str .= '<div class="hero-comparison-section"><div class="progress-category">';
		foreach($data->competitivePlay->heroes as $hero)
		{
			$str .= '<div class="progress-category-item">';
				$str .= '<img src="' . $hero->photo . '" />';
				$str .= '<div class="title">' . $hero->name . '</div>';
				$str .= '<div class="description">' . $hero->minutes . '</div>';
			$str .= '</div>';
		}
		$str .= '</div></div>';
		
		$str .= '</div>';

		$str .= '</cache>';
		
		file_put_contents($file, $str);
	}
	
}
