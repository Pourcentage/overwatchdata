<?php

require_once('./simplehtmldom/simple_html_dom.php');
require_once('OverwatchCacheWriter.class.php');
require_once('OverwatchData.class.php');
require_once('OverwatchHeroData.class.php');

/**
 * Récupère les données depuis une page de stats d'Overwatch
 */
class OverwatchParser
{	
	private $html = null;
	private $data = null;
	private $cacheFile = '';
	private $is404 = false;

	/**
     * @param string $playerId Identifiant du joueur (exemple : abcd-1234)
     * @param string $region Région du monde
     * @param string $platform pc, xbox, game boy color, etc.
	 * @param int $cacheDuration Durée de vie du cache (en secondes)
     */
	public function __construct($playerId, $region = 'eu', $platform = 'pc', $cacheDuration = 3600)
	{
		$cacheDir = 'cache';
		if(!is_dir($cacheDir)){
		   mkdir($cacheDir);
		}

		$lang = 'en-us';
		$url = 'https://playoverwatch.com/' . $lang . '/career/' . $platform . '/' . $region . '/' . $playerId;
		$this->cacheFile = 'cache/' . str_replace('/', '_', $url) . '.html';
		$this->cacheFile = str_replace(':', '', $this->cacheFile);
		
		$headers = @get_headers($url);
		if(strpos($headers[0],'404') !== false)
		{
			$this->is404 = true;
		}

		if(!$this->is404)
		{
			$this->html = new simple_html_dom();
			if(file_exists($this->cacheFile))
			{
				$cacheFileContent = file_get_contents($this->cacheFile);
				if((time() - filemtime($this->cacheFile)) > $cacheDuration || $cacheFileContent == false || $cacheFileContent == '') // 1 heure ou fichier de cache vide
				{
					unlink($this->cacheFile);
				}
				else
				{
					$this->html->load_file($this->cacheFile);
				}
			}
			if(!file_exists($this->cacheFile))
			{
				@$this->html->load_file($url);
			}
		}
	}

	/**
	 * Récupère les données et les renvoie ou renvoie null si le joueur n'existe pas
	 * @return OverwatchData|null
	 */
	public function parse()
	{
		if($this->is404)
			return null;
	
		$this->data = new OverwatchData();
		// General
		$this->data->playerName = $this->html->find('h1.header-masthead', 0)->innertext;
		$this->data->playerLevel = $this->html->find('.player-level div', 0)->innertext;
		$this->data->playerLevelIcon = strtok(explode('(', $this->html->find('.player-level', 0)->getAttribute('style'))[1], ')');
		$this->data->competitiveRank = $this->html->find('.competitive-rank div', 0)->innertext;
		$this->data->gamesWon = strtok($this->html->find('p.masthead-detail span', 0)->innertext, ' ');

		// Quick Play
		$this->multipleData('#quickplay', $this->data->quickPlay);
		// Competitive Play
		$this->multipleData('#competitive', $this->data->competitivePlay);
		
		if(!file_exists($this->cacheFile))
		{
			$cacheWriter = new OverwatchCacheWriter();
			$cacheWriter->write($this->data, $this->cacheFile);
		}
		
		return $this->data;
	}
	
	/**
	 * Récupère les données pour un type de partie
	 * @param baseId : Sélecteur (exemple : '#quickplay')
	 * @param obj : OverwatchPlayData
	 */
	private function multipleData($baseId, $obj)
	{
		$cardsHeading = $this->html->find($baseId . ' .card-heading');

		$obj->eliminationsAverage = $cardsHeading[0]->innertext;
		$obj->damageDoneAverage = $cardsHeading[1]->innertext;
		$obj->deathsAverage = $cardsHeading[2]->innertext;
		$obj->finalBlowsAverage = $cardsHeading[3]->innertext;
		$obj->healingDoneAverage = $cardsHeading[4]->innertext;
		$obj->objectiveKillsAverage = $cardsHeading[5]->innertext;
		$obj->objectiveTimeAverage = $cardsHeading[6]->innertext;
		$obj->soloKillsAverage = $cardsHeading[7]->innertext;
		
		$items = $this->html->find($baseId . ' .hero-comparison-section .progress-category', 0);
		$items = $items->find('.progress-category-item');
		foreach($items as $i)
		{
			$hero = new OverwatchHeroData();
			$hero->name = $i->find('.title', 0)->innertext;
			$hero->photo = $i->find('img', 0)->getAttribute('src');
			$timeValue = $i->find('.description', 0)->innertext;
			if(strpos($timeValue, 'hour') !== false || strpos($timeValue, 'heure') !== false) // Ce sont des heures
				$timeValue = intval($timeValue) * 60;
			$hero->minutes = intval($timeValue);
			$obj->heroes[] = $hero;
		}
	}
	
}
