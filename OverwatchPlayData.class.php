<?php

/**
 * Stats diverses lors des parties
 */
class OverwatchPlayData
{
	/**
	 * Moyenne d'éliminations
	 */
	public $eliminationsAverage = '';
	/**
	 * Moyenne de dégâts infligés
	 */
	public $damageDoneAverage = '';
	/**
	 * Moyenne de morts
	 */
	public $deathsAverage = '';
	/**
	 * Moyenne de coups de grâce
	 */
	public $finalBlowsAverage = '';
	/**
	 * Moyenne de soins prodigués
	 */
	public $healingDoneAverage = '';
	/**
	 * Moyenne de victimes sur objectif
	 */
	public $objectiveKillsAverage = '';
	/**
	 * Moyenne de temps sur objectif
	 */
	public $objectiveTimeAverage = '';
	/**
	 * Moyenne de victimes en solo
	 */
	public $soloKillsAverage = '';
	
	/**
	 * Liste des héros du plus joué au moins joué
	 * @var array(OverwatchHeroData)
	 */
	public $heroes = array();

}

