<?php

/**
 * Données d'un Héros
 */
class OverwatchHeroData
{
	/**
	 * Nom du héros
	 */
	public $name = '';
	/**
	 * URL de la photo du héros
	 */
	public $photo = '';
	/**
	 * Minutes de jeu avec ce héros
	 */
	public $minutes = '';
	
	/**
	 * Grande image du héros
	 * @return string URL de la grande image du héros
	 */
	public function bigImage()
	{
		$n = strtolower($this->name);
		switch($n)
		{
			case 'faucheur':
				$n = 'reaper';
				break;
			case 'soldat : 76';
				$n = 'soldier: 76';
				break;
			case 'chacal':
				$n = 'junkrat';
				break;
			case 'fatale':
				$n = 'widowmaker';
				break;
			case 'chopper':
				$n = 'roadhog';
				break;
			case 'ange':
				$n = 'mercy';
				break;
			default:
				break;
		}
		$n = str_replace(':', '-', $n);
		$n = str_replace('.', '', $n);
		$n = str_replace(' ', '', $n);
		$n = str_replace('ö', 'o', $n);
		$n = str_replace('ú', 'u', $n);
		$url = 'https://blzgdapipro-a.akamaihd.net/hero/' . $n . '/career-portrait.png';
		return $url;
	}

}
