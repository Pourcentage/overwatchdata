<?php
require_once('OverwatchParser.class.php');
$op = new OverwatchParser('kozika-2190');
// $op = new OverwatchParser('Fadden-2118');
// $op = new OverwatchParser('mikk017f-2536');
$data = $op->parse();
?>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>OverwatchParser test</title>
</head>

<body>
	<?php
		if($data != null)
		{
			echo '<ul>';
			echo '<li>' . $data->playerName . ' : ' . $data->competitiveRank . '</li>';
			echo '<li>' . $data->quickPlay->healingDoneAverage . '</li>';
			echo '<li>' . $data->quickPlay->soloKillsAverage . ' - ' . $data->competitivePlay->soloKillsAverage . '</li>';
			echo '</ul>';
			echo '<img src="' . $data->playerLevelIcon . '" alt="Level" />';
			echo '<img src="' . $data->competitivePlay->heroes[0]->bigImage() . '" alt="' . $data->competitivePlay->heroes[0]->name . '" />';
			echo '<img src="' . $data->competitivePlay->heroes[0]->photo . '" alt="' . $data->competitivePlay->heroes[0]->name . '" />';
			echo '<p>' . $data->quickPlay->heroes[1]->minutes . '</p>';
			echo '<p>' . $data->gamesWon . '</p>';
		}
		else
		{
			echo '<p>Le joueur n\'existe pas.</p>';
		}
	?>
</body>

</html>
