<?php

require_once('OverwatchPlayData.class.php');

/**
 * Données récupérées
 */
class OverwatchData
{
	/**
	 * Nom du joueur
	 */
	public $playerName = '';
	/**
	 * Niveau du joueur
	 */
	public $playerLevel = '';
	/**
	 * URL de l'icône du niveau du joueur
	 */
	public $playerLevelIcon = '';
	/**
	 * Rang compétitif
	 */
	public $competitiveRank = '';
	/**
	 * Nombre de parties remportées
	 */
	public $gamesWon = '';

	/**
	 * Stats en QuickPlay
	 * @var OverwatchPlayData
	 */
	public $quickPlay = null;
	/**
	 * Stats en CompetitivePlay
	 * @var OverwatchPlayData
	 */
	public $competitivePlay = null;
	
	public function __construct()
	{
		$this->quickPlay = new OverwatchPlayData();
		$this->competitivePlay = new OverwatchPlayData();
	}
	
}

